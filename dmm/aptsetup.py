"""
aptsetup functions for for dmm.
"""

import os
import subprocess


def init():
    """
    Initialization for aptsetup module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for aptsetup module
    """
    text_file = open(config['chroot'] + config['sources-file'], "w")
    sources_write = text_file.write(config['sources-list'])
    text_file.close()
    if config['update-sources']:
        result = subprocess.run("chroot %s apt-get -q update" % (config['chroot']),
                                capture_output=True, text=True, shell=True)
        print(result.stdout)
        return result.returncode


init()
