"""
Generate checksums module for dmm.
"""

import os

def generate_checksums_file(sumbinary, path, output, cut):
    """
    Create a file that contains checksums:

    - sumbinary: md5sum, sha1sum, sha256sum
    - path: path that will be checksummed
    - output: where the checksums will be outputted to
              if empty, the result is returned
    - exclude: a list of lines that will include these
               strings will be excluded
    """
    checksum_output = os.popen("find %s -type f | xargs %s" % (path, sumbinary)).read()
    checksum_output_cut = checksum_output.replace(cut, '')
    if output == "":
        return (checksum_output_cut)
    else:
        file = open(output, "w+")
        file.write(checksum_output_cut)
        file.close()


def confirm_checksums(file):
    #TODO
    pass
