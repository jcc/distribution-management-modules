"""
Python wrapper for dd, usually to create large empty disk image
"""

import subprocess

def create_file(ifile, ofile, bs, count, timeout):
    """
    Create or overwrite a file using dd.

    As with dd:

    - ifile: inputfile
    - ofile: outputfile
    - bs: size of block
    - count: how many blocks should be written
    - timeout: make the operation timeout after amount of seconds
    """
    dd_output = subprocess.run(["/usr/bin/dd",
                        "if=" + ifile,
                        "of=" + ofile,
                        "bs=" +  bs,
                        "count=" + count],
                        check=True,
                        timeout=timeout,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                        universal_newlines=True,)
    return (dd_output.returncode, dd_output.stdout, dd_output.stderr)

