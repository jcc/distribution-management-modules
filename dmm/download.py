"""
download remote content for dmm
"""

import os
import pathlib

def init():
    """
    Initialization for download module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for download module
    """
    if config['action'] == 'download_files':
        for download in config['files']:
            download_file(download['url'], download['destination'])


def download_file(url, destination):
    """
    Download file using curl and store it at location
    """
    print("Downloading %s to %s" % (url, destination))
    # Ensure that base path exists
    path = pathlib.Path(destination).parent
    path.mkdir(parents=True, exist_ok=True)
    os.system("curl %s -o %s" % (url, destination))


init()
