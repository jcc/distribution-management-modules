"""
dpkgsetup functions for for dmm.
"""

import os

def init():
    """
    Initialization for dpkgsetup module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for dpkgsetup module
    """
    text_file = open(config['chroot'] + config['options-file'], "w")
    options_write = text_file.write(config['dpkg-options'])
    text_file.close()


init()
