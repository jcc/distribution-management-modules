"""
fallocate functions for dmm.
"""

import os

def init():
    """
    Initialization for fallocate module
    """
    pass


def create_file(size, destination):
    """
    Create an empty sparse file.
    """
    os.system("fallocate -l %s %s" % (size, destination))


def recipe_run(config, globalconf):
    """
    Perform actions for fallocate module
    """
    create_file(config['size'], config['destination'])


init()
