""" Wrapper around fdisk """
import os


from subprocess import (PIPE, Popen)


def invoke(command):
    '''
    Invoke process and return its output.
    '''
    return Popen(command, stdout=PIPE, shell=True).stdout.read()


def list_disks():
    disklist = invoke('fdisk -l')
    print(disklist)
