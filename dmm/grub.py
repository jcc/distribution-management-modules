"""
grub functions for for dmm.
"""

import os

def init():
    """
    Initialization for grub module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for grub module
    """
    if config['action'] == "install":
        os.system("chroot %s apt-get -y -qq install grub-%s" % (config['chroot'], config['platform']))
        os.system("chroot %s update-grub" % config['chroot'])
        if config['platform'] == "efi":
            return os.system("chroot %s grub-install --target=x86_64-efi" % config['chroot'])
    if config['action'] == "mkrescue":
        return os.system('grub-mkrescue --modules="multiboot fat linux part_msdos minicmd ls iso9660" --output="%s" %s' % (config['isopath'], globalconf["live-media-path"]))
    if config['action'] == "create_legacy_boot_img":
        setup_grub_legacy_boot(globalconf["live-media-path"])


def setup_grub_legacy_boot(path):
    """
    Create PC BIOS (amd64/i386 legacy) bootable grub image.
    """
    # TODO: should probably happen inside the chroot?
    os.system('grub-mkstandalone --directory=/usr/lib/grub/i386-pc --format=i386-pc '
              '--themes="" --fonts="" --locales="" --modules="linux normal iso9660 '
              'biosdisk search png gfxmenu" --install-modules="linux normal iso9660 '
              'biosdisk memdisk search tar ls png gfxmenu" --output core.img')
    os.system('cat /usr/lib/grub/i386-pc/cdboot.img core.img > bios.img')


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'xorriso': {"priority": "required", \
             "description": "command line ISO-9660 and Rock Ridge manipulation tool"},
             'mtools': {"priority": "required", \
             "description": "Tools for manipulating MSDOS files"}
           })

init()
