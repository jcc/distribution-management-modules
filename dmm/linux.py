"""
mkfs functions for for dmm.
"""

import os

def init():
    """
    Initialization for linux module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for linux module
    """
    os.system("chroot %s apt-get -y -qq install %s" % (config['chroot'], config['package']))


init()
