"""
livedisk functions for for dmm.
"""

import pathlib
import os
from shutil import copyfile

def init():
    """
    Initialization for livedisk module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for livedisk module
    """
    # Create working directory for live media
    path = pathlib.Path(globalconf['live-media-path'] + "/live")
    path.mkdir(parents=True, exist_ok=True)

    # copy squashfs, kernel and initramfs into livedir
    print("Current working directory is " + os.getcwd())
    squashfs = globalconf['squashfs']
    kernel = globalconf['chroot'] +  "/boot/vmlinuz*"
    initrd = globalconf['chroot'] + "/boot/initrd*"
    livepath = globalconf['live-media-path'] + "/live/"
    os.system("cp %s %s" % (squashfs, livepath + "/filesystem.squashfs"))
    os.system("cp %s %s" % (kernel, livepath + "/vmlinuz"))
    os.system("cp %s %s" % (initrd, livepath + "/initrd.gz"))

    # Copy template directory to live media
    teamplate_dir = config['template_dir']
    os.system("cp -r %s/ %s" % (config['template_dir'] + "/*", globalconf['live-media-path']))

    # Update initramfs
    if config['update-initramfs']:
        os.system("chroot %s update-initramfs -u" % globalconf['chroot'])

    # TODO: create md5sums



init()
