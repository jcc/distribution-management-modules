"""
losetup functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Perform actions for losetup module
    """
    if config['function'] == 'unmount':
        os.system("losetup -d /dev/%s" % config['loopdev'])
    else:
        os.system("losetup -P /dev/%s %s" % (config['loopdev'], config['diskimg']))


