"""
Show disk / partition information.
"""

import subprocess
import json

def list_scsi_devices():
    """
    Just return a list of available scsi disks.
    """
    lsblk_output = subprocess.check_output(["/bin/lsblk", "--scsi",
                                            "-noheadings", "--output",
                                            "NAME", "-n"]).decode("utf-8").split("\n")
    scsi_devices = [x for x in lsblk_output if x]
    return scsi_devices


def list_block_devices():
    """
    Get json data from lsbkl and convert it to a python dict.
    """
    lsblk_output = subprocess.check_output(["/bin/lsblk", "-J", "-O", "-b"])
    json_data = json.loads(lsblk_output)
    print(json_data)
    return(json_data)


def get_device_information(device):
    """
    Get ..."
    """


def list_partitions():
    """
    TODO: We need to list the partitions somehow
    so that we can present a disk to run cfdisk on.
    """
