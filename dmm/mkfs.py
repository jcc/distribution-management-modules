"""
mkfs functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Perform actions for mkfs module
    """
    for filesystem in config['partitions']:
        os.system("mkfs.%s %s %s" % (filesystem['fstype'], filesystem['options'], filesystem['partition']))


