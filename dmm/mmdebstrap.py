"""
mmdebstrap functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Perform actions for mmdebstrap module
    """
    os.system("mmdebstrap %s %s %s %s" % (config['debootstrapopts'],
                                                    config['release'],
                                                    config['destination'],
                                                    config['mirror']))


