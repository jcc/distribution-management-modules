"""
mountfs functions for for dmm.
"""

import os
import pathlib


#def recipe_run(config, globalconf):
#    """
#    Perform actions for mountfs module
#    """
#    print(config['mount'])
#    for filesystem in config['mount']:
#        print(config['mount'][filesystem]['mountpoint'])
#        path = pathlib.Path(config['mount'][filesystem]['mountpoint'])
#        path.mkdir(parents=True, exist_ok=True)
#        os.system("mount -t %s %s %s %s" % (config['mount'][filesystem]['fstype'],
#                                            config['mount'][filesystem]['mountopts'],
#                                            config['mount'][filesystem]['source'],
#                                            config['mount'][filesystem]['mountpoint']))

def recipe_run(config, globalconf):
    """
    Perform actions for mountfs module
    """
    for filesystem in config['partitions']:
        path = pathlib.Path(filesystem['mountpoint'])
        path.mkdir(parents=True, exist_ok=True)
        os.system("mount -t %s %s %s %s" % (filesystem['fstype'], filesystem['mountopts'],
                  filesystem['source'], filesystem['mountpoint']))

