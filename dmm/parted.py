"""
parted functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Creates partitions on device.
    """
    for partedcmd in config['static-layout']:
        os.system("parted -s -- %s %s" % (config['destination'], partedcmd))

