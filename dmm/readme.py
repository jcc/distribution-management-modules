"""
readme functions for for dmm.
"""

import os
from datetime import date
# TODO: the date should come from our framework

def recipe_run(config, globalconf):
    """
    Perform actions for readme module
    """
    template = config['template']
    destination = config['destination'] + "/"
    filename = config['filename']
    product = config['name']
    version = config['version']
    readme = open(template).read().replace("__PRODUCT__", product).replace("__VERSION__", version).replace("__DATE__", str(date.today()))
    readme_w = open(destination + filename, 'w')
    readme_w.write(readme)
    readme_w.close()


