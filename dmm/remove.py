"""
delete functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Perform recipe actions for delete module
    """
    for path in config['paths']:
        delete_path(path['path'])


def delete_path(path):
    print("Removing %s" % path)
    os.system("rm -rf %s" % path)

