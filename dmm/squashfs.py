"""
mksquashfs functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Perform actions for mksquashfs module
    """
    if config['action'] == 'make-squashfs':
        blocksize = config['blocksize']
        compress = config['compression-method']
        source = config['source']
        dest = config['destination']
        opts = config['options']
        os.system("mksquashfs %s %s -b %s -comp %s %s" % (source, dest, blocksize, compress, opts))

    #if config['action'] == unsquashfs:
        # todo

