"""
umountfs functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Perform actions for umountfs module
    """
    for mount in config['mounts']:
        os.system("umount %s" % (mount['mountpoint']))

