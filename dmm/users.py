"""
create users, currently only in chroot
"""

import os


def recipe_run(config, globalconf):
    """
    Perform actions for users module
    """
    for user in config['users']:
        #print("username: " + user['username'])
        #print("chroot: " + globalconf['chroot'])
        password_hash = os.popen("mkpasswd --method=SHA-512 " + user['password']).read().replace('\n', '')
        os.system("chroot %s useradd -p '%s' %s" % (globalconf['chroot'],password_hash, user['username']))
        os.system("chroot %s mkdir home/%s" % (globalconf['chroot'], user['username']))
        os.system("chroot %s chown %s home/%s" % (globalconf['chroot'], user['username'], user['username']))
        if user['sudo']:
            os.system("chroot %s adduser %s sudo" % (globalconf['chroot'], user['username']))
            os.system("chroot %s apt-get install sudo" % globalconf['chroot'])

