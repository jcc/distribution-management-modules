"""
vmsetup functions for for dmm.
"""

import os

def recipe_run(config, globalconf):
    """
    Misc setup tasks for virtual machines
    """
    chroot = config['chroot']
    os.system("sed -i 's/%s/%s/g' %s/boot/grub/grub.cfg" % (config['root-during-build'],
                                                            config['root-dev-in-vm'],
                                                            chroot))
    os.system("echo '/dev/%s / ext4 defaults 0 0' > %s/etc/fstab" % (config['root-dev-in-vm'], chroot))

    # qemu's bios only reads the EFI files if it can find them in the right place, and
    # vfat doesn't support symlinks
    os.system("cp -r %s/boot/efi/EFI/debian %s/boot/efi/EFI/boot" % (chroot, chroot))
    os.system("cp -r %s/boot/efi/EFI/boot/grubx64.efi %s/boot/efi/EFI/boot/bootx64.efi" % (chroot, chroot))
    os.system(("sed -i 's/quiet/quiet console=ttyS0/g' %s/etc/default/grub") % chroot)
    os.system("chroot %s /usr/sbin/update-grub" % chroot)

