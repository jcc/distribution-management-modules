"""
xorisso functions for dmm (not yet implemented)
"""

import os

def recipe_run(config, globalconf):
    """
    xorrisso actions:

     - geniso generates an iso image
    """
    if config['action'] == "geniso":
        generate_iso(config["isopath"], config["live-media-path"])


def generate_iso(isopath, live_media_path):
    
    """
    Generate an ISO image using xorriso
    """
    print("xorriso -outdev %s -volid d-live testing ci amd64 -padding 0 -compliance no_emul_toc -map %s -chmod 0755 / -- -boot_image isolinux dir=/isolinux -boot_image isolinux system_area=/usr/lib/grub/i386-pc/boot_hybrid.img -boot_image any next -boot_image any efi_path=boot/grub/efi.img -boot_image isolinux partition_entry=gpt_basdat % (isopath, live-media-path)")

