#!/usr/bin/env dmm-perform-recipe

module_path:
- local

global_settings:
    name: &name Debian
    version: &version bookworm (bookworm)
    description: &description Live Xfce installer image (bookworm)
    live-media-path: &live-media-path /tmp/debmower/live-media
    architecture: &architecture amd64
    workspace: /tmp/debmower/
    chroot: &chroot /tmp/debmower/disk
    squashfs: &squashfs /tmp/debmower/filesystem.squashfs
    release: &release bookworm
    apt_depends: &apt_depends debootstrap reprepro squashfs-tools mtools xorriso curl grub-pc-bin


recipe:
    install_dependencies:
        module: aptpkg
        action: install
        packages: *apt_depends
        clean_cache: False
        download_only: False
        chroot: False
    install_debian:
        module: debootstrap
        destination: *chroot
        release: *release
        mirror: http://deb.debian.org/debian
        debootstrapopts: ''
#    setup_proxy:
#        module: writefile
#        action: write_files
#        files:
#            - proxy:
#              content: Acquire::http::proxy "http://localhost:3142";
#              destination: /tmp/debmower/disk/etc/apt/apt.conf.d/02proxy
    setup_network:
        module: networking
        chroot: *chroot
        hostname: live
    apt_setup:
        module: aptsetup
        chroot: *chroot
        sources-list: |
            deb http://deb.debian.org/debian bookworm main
            deb http://deb.debian.org/debian bookworm main/debian-installer
        sources-file: /etc/apt/sources.list
        update-sources: True
    setup_bind_mounts:
        module: mountfs
        partitions:
            - dev:
              source: /dev
              mountpoint: /tmp/debmower/disk/dev
              mountopts: ''
              fstype: devtmpfs
            - proc:
              source: /proc
              mountpoint: /tmp/debmower/disk/proc
              mountopts: ''
              fstype: proc
            - sys:
              source: /sys
              mountpoint: /tmp/debmower/disk/sys
              mountopts: ''
              fstype: sysfs
    install_main_meta:
        module: aptpkg
        action: install
        packages: live-task-xfce
        clean_cache: True
        download_only: False
        chroot: *chroot
    setup_live_packages:
        module: aptpkg
        action: install
        packages: calamares calamares-settings-debian live-boot live-config-systemd live-tools user-setup live-config live-boot-initramfs-tools grub-common live-task-recommended live-task-localisation grub-common grub-efi-amd64 grub-pc-bin
        clean_cache: True
        download_only: False
        chroot: *chroot
    install_linux:
        module: linux
        chroot: *chroot
        package: linux-image-amd64
    download_pool_packages:
        module: aptpkg
        action: install
        packages: grub-efi efibootmgr grub-efi-amd64 grub-efi-amd64-bin grub-pc-bin grub-pc
        clean_cache: False
        download_only: True
    download_pool_udeb_packages:
        module: aptpkg
        action: install
        packages: netcfg ethdetect pcmciautils-udeb live-installer udpkg
        clean_cache: False
        download_only: True
    setup_package_pool:
        module: reprepro
        prepend_path: *chroot
        package_path: /var/cache/apt/archives
        architectures: *architecture
        description: *description
        components: main
        live-media-path: *live-media-path
        clean_package_path: True
    clean_apt:
        module: aptsetup
        chroot: *chroot
        sources-list: ''
        sources-file: /etc/apt/sources.list
        update-sources: True
    clean_chroot_environment:
        module: remove
        action: delete
        paths:
            - proxyconf:
              path: /tmp/debmower/disk/etc/apt/apt.conf.d/02proxy
    unmount_filesystems:
        module: umountfs
        mounts:
            - proc:
              mountpoint: /tmp/debmower/disk/proc
            - sys:
              mountpoint: /tmp/debmower/disk/sys
            - dev:
              mountpoint: /tmp/debmower/disk/dev
    make_squashfs:
        module: squashfs
        action: make-squashfs
        blocksize: 131072
        compression-method: zstd
        source: *chroot
        destination: *squashfs
        options: -noappend
    download_debian_installer:
        module: download
        action: download_files
        files:
            - initrd.gz:
              url: https://d-i.debian.org/daily-images/amd64/daily/cdrom/initrd.gz
              destination: /tmp/debmower/live-media/boot/d-i/initrd.gz
            - vmlinuz:
              url: https://d-i.debian.org/daily-images/amd64/daily/cdrom/vmlinuz
              destination: /tmp/debmower/live-media/boot/d-i/vmlinuz
            - gtk-initrd.gz:
              url: https://d-i.debian.org/daily-images/amd64/daily/cdrom/gtk/initrd.gz
              destination: /tmp/debmower/live-media/boot/d-i/gtk/initrd.gz
            - gtk-vmlinuz:
              url: https://d-i.debian.org/daily-images/amd64/daily/cdrom/gtk/initrd.gz
              destination: /tmp/debmower/live-media/boot/d-i/gtk/vmlinuz
    write_disk_metadata:
        module: writefile
        action: write_files
        files:
           - base_components:
             content: main
             destination: /tmp/debmower/live-media/.disk/base_components
           - base_installable:
             content: ''
             destination: /tmp/debmower/live-media/.disk/base_installable
           - cd_type:
             content: live
             destination: /tmp/debmower/live-media/.disk/cd_type
           - info:
             content: Debian GNU/Linux Live FIXME 0.12.0.0 "Bookworm" - UnOfficial amd64 20220326-11:22 #FIXME
             destination: /tmp/debmower/live-media/.disk/info
           - udeb_include:
             content: |
                 netcfg
                 ethdetect
                 pcmciautils-udeb
                 live-installer
             destination: /tmp/debmower/live-media/.disk/udeb_include
    prepare_live_environment:
        module: livedisk
        template_dir: './debian_template'
        update-initramfs: True
    generate_readme:
        module: readme
        template: ./debian_template/README.txt
        destination: *live-media-path
        filename: README.txt
        name: *name
        version: *version
    make_iso_fs:
        module: grub
        action: mkrescue
        isopath: /tmp/debian-live-12-dmm-xfce.iso

