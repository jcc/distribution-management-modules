import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dmm-highvoltage",
    version="0.1.1",
    author="Jonathan Carter",
    author_email="jcc@debian.org",
    description="Distribution management modules",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://salsa.debian.org/highvoltage/dmm",
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: ISC License (ISC)",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires='>=3.6',
)

