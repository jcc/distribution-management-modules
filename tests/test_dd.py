#!/usr/bin/python3
from dmm.modules.dd import dd
import os

def test_dd_create_file():
    dd.create_file("/dev/zero", "/tmp/test.img", "2M", "2", 2)
    file_stats = os.stat("/tmp/test.img")
    # 2M * 2 should equal 4M
    assert file_stats.st_size == 4194304
    os.remove("/tmp/test.img")
